// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall_BP.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
AWall_BP::AWall_BP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWall_BP::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall_BP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall_BP::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy(true, true);
			Snake -> Life--;
		}
	}
}


